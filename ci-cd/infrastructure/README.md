# Welcome to your CDK TypeScript project!

This is a blank project for TypeScript development with CDK.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Useful commands

- `npm run build` compile typescript to js
- `npm run watch` watch for changes and compile
- `npm run test` perform the jest unit tests
- `cdk deploy` deploy this stack to your default AWS account/region
- `cdk diff` compare deployed stack with current state
- `cdk synth` emits the synthesized CloudFormation template

## For deployment

- Required params for deployment docker image version - tag version or `${branchName}-${commitId}`

```sh
npm run build
STACK_NAME=felipeloha-aws-shopify-app-infra CDK_DEFAULT_ACCOUNT=262360674382 CDK_DEFAULT_REGION=ap-southeast-1 cdk synth
STACK_NAME=felipeloha-aws-shopify-app-infra cdk deploy --parameters version=dev-tommy-001-test-cicd-0dede65
```
