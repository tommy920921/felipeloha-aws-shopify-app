FROM node:12-alpine

ENV NODE_ENV production

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN yarn install --forzen-lockfile
RUN yarn build

EXPOSE 8081

USER node
CMD yarn run start
